#!/bin/bash

PORT=25565

function start_service ()
{
    echo "Starting service"
}

function stop_service ()
    echo "Stopping service"
    aws lambda invoke --function-name stop-function --payload '{ "key": "value" }' response.json
    exit 0
}

function update_service ()
{
    echo "Updating DNS records"
    aws lambda invoke --function-name update-dns-function --payload '{ "key": "value" }' response.json
}

function sigterm ()
{
    echo "Received SIGTERM"
    stop_service
}
trap sigterm SIGTERM

TIMEOUT=600
COUNTER=0
echo "Waiting for Minecraft java server"
while true
do
  netstat -atn | grep ":${PORT}" | grep LISTEN && break
  sleep 1
  COUNTER=$(($COUNTER + 1))
  if [ $COUNTER -gt $TIMEOUT ]
  then
    echo "Timeout after $TIMEOUT seconds with no server"
    stop_service
  fi
done
echo "Listening after $COUNTER seconds"

echo "Waiting for Minecraft RCON"
COUNTER=0
while [ $STARTED -lt 1 ]
do
  CONNECTIONS=$(netstat -atn | grep :25575 | grep LISTEN | wc -l)
  STARTED=$(($STARTED + $CONNECTIONS))
  if [ $STARTED -gt 0 ]
  then
    echo "RCON is listening, we are ready for clients."
    break
  fi
  sleep 1
  COUNTER=$(($COUNTER + 1))
  if [ $COUNTER -gt $TIMEOUT ]
  then
    echo "Timeout after $TIMEOUT seconds with no RCON"
    stop_service
  fi
done

start_service
