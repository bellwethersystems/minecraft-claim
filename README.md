# Minecraft Claim (MCC)

MCC is an AWS stack for launching DNS-based Minecraft worlds on-demand.

Stacks are unique per domain name and represent a single persistent Minecraft world. Stacks are created using CloudFormation templates and luanch all the necessary network and service infrastructure scoped for the given domain.

> NOTE: domain names must be verified in AWS before they can be used in the stack.

#### Requirements

+ AWS account
+ Registered domain name(s)
+ Docker
+ Make

#### Architecture

Rather than using a conventional control plane to provision infrastructure (which incurs continuous costs), the stack monitors CloudWatch logs for DNS queries and scales an ECS cluster to a capacity of 1 in response. As a result, there is a cold start time when no server is running.

When logging in from a Minecraft client, this means that the first attempt against an inactive server will fail, and must be retried. Alternatively, a domain staus page can be visited to start the server by performing DNS queries against the Cloudfront Web Distribution.

## Local Development

Make is used to for dockerizing the app, including running, building, tagging, and publising the app to an ECR rregistry. Execute make with no args to print out the available commands and environment variables:

    make -C ./docker

## AWS Deployment

All MCC infrastructure is managed and deployed through CloudFormation. The respective templates can be found in the [cfn directory](cfn/).

### Image Registry Stack

[This template](./cfn/stack.cform.yaml) creates a private Docker registry for the Minecraft server images used by ECS.

### Server Domain Stack

[This template](./cfn/stack.cform.yaml) creates an ECS cluster that scales from 0-1 servers based on server activity. When no servers are running, a DNS request triggers a Lambda Function to upscale to 1 server. Once a server is active, it will monitor the minecraft server connections and trigger a Lambda Function to downscale to 0 servers once there are no more active connections. 

Stack resources are prefixed with the stack name so it's best to use a consistent naming convention, e.g. the domain, world name, etc.

This is an *instance template* and one stack should be craeted in CloudFormation per Minecraft world/domain.

### Status Management Stack

[This template](./cfn/status.cofrm.yaml) creates a static SPA status page that is used to perform DNS queries to initialize the Minecraft server and verify server status.

This is a *global template* and one stack should be created in CloudFormation to be used by all server stacks; the status page will be made available at the parameterized domain URL, e.g. https://example.com/status.

## Services

The following services are used to build MCC:

+ CloudFormation
+ CloudFront
+ CloudWatch
+ Elastic Container Registry (ECR) 
+ Elastic Container Service (ECS)
+ Elastic File System (EFS) 
+ Identity and Access Management (IAM)
+ Lambda
+ Route53
+ Simple Storage Service (S3)
+ Virtual Private Cloud (VPC)
